package telran.tests;

import org.junit.jupiter.api.Test;
import telran.ArrayListNumbersBox;
import telran.NumbersBox;

import java.util.Arrays;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


class AnyNumbersTests {

    int[]      numbers    = { 888, 1, 55, 2, 222, 3, 11, 122, 777, 222, 10, 98, 222, 75, 55, 999, 100 };
    NumbersBox collection = new ArrayListNumbersBox ( );

    private void add ( NumbersBox collection ) {
        for ( Integer number : numbers ) {
            collection.addNumber ( number );
        }
    }


//    NumbersBox collection =  LinkedListNumbersBox();
//    NumbersBox collection =  TreeSetNumbersBox();
//    NumbersBox collection =  HashSetNumbersBox();

    @Test
    void RemoveAll ( ) {
        add ( collection );
        int removed = collection.removeNumbersInRange ( 0, 1000 );
        assertEquals ( 17, removed );
        assertEquals ( 0, collection.length ( ) );
    }

    @Test
    void testRemoveRepeated ( ) {
        add ( collection );
        int length  = collection.length ( );
        int removed = collection.removeRepeated ( );
        assertEquals ( collection.length ( ), length - removed );
    }

    @Test
    void testRangeRemove ( ) {
        add ( collection );
        int removed = collection.removeNumbersInRange ( 0, 100 );
        assertEquals ( 10, removed );
    }

    @Test
    void testSingleRemove ( ) {
        add ( collection );
        int removeNumber = 777;

        collection.removeNumber ( removeNumber );

        for ( Integer value : collection ) assertNotEquals ( value, removeNumber );

        removeNumber = 888;
        collection.removeNumber ( removeNumber );

        for ( Integer value : collection ) assertNotEquals ( value, removeNumber );

        removeNumber = 999;
        collection.removeNumber ( removeNumber );

        for ( Integer value : collection ) assertNotEquals ( value, removeNumber );
    }

    @Test
    void testIterator ( ) {
        add ( collection );

        int[] numbers = getNumbers ( collection );

        Arrays.sort ( numbers );
        Arrays.sort ( this.numbers );

        IntStream.range ( 0, this.numbers.length ).forEach ( i -> assertEquals ( this.numbers[ i ], numbers[ i ] ) );
        assertEquals ( this.numbers.length, numbers.length );

    }

    private int[] getNumbers ( NumbersBox collection ) {
        int[] res = new int[ collection.length ( ) ];

        int index = 0;
        for ( Integer value : collection ) {
            res[ index++ ] = value;
        }

        return res;
    }
}