package telran;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public abstract class RemoveList extends anyNumbersBox {
    @Override
    public int removeRepeated ( ) {
        Collection<Integer> oldNumbers = numbers;
        numbers = newCollection ( );

        Collection<Integer> integers     = numbers;
        Set<Integer>        values = new HashSet<> ( );
        for ( Integer old : oldNumbers ) {
            if ( values.add ( old ) ) {
                integers.add ( old );
            }
        }

        return oldNumbers.size ( ) - length ( );
    }
}