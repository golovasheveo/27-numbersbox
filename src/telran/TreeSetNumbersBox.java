package telran;

import java.util.Collection;
import java.util.TreeSet;

public class TreeSetNumbersBox extends RemoveSet {
    @Override
    protected Collection<Integer> newCollection ( ) {
        return new TreeSet<> ( );
    }

    @Override
    public int removeNumbersInRange ( int from, int to ) {
        TreeSet<Integer> oldData = (TreeSet<Integer>) numbers;

        numbers = newCollection ( );
        numbers.addAll ( oldData.headSet ( from, false ) );
        numbers.addAll ( oldData.tailSet ( to, false ) );

        return oldData.size ( ) - numbers.size ( );
    }
}