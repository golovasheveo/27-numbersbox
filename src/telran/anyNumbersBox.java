package telran;

import java.util.Collection;
import java.util.Iterator;

public abstract class anyNumbersBox implements NumbersBox {

    protected Collection<Integer> numbers;

    public anyNumbersBox ( ) {
        this.numbers = newCollection ( );
    }

    @Override
    public void addNumber ( int number ) {
        numbers.add ( number );
    }

    @Override
    public void removeNumber ( int number ) {
        numbers.remove ( number );
    }

    @Override
    public Iterator<Integer> iterator ( ) {
        return numbers.iterator ( );
    }

    @Override
    public int removeNumbersInRange ( int from, int to ) {
        int                 tlength = length ( );
        Collection<Integer> buffer  = newCollection ( );

        for ( Integer value : numbers ) {
            if ( value < from ) buffer.add ( value );
            if ( value > to ) buffer.add ( value );
        }
        numbers = buffer;
        return tlength - length ( );
    }

    @Override
    public int length ( ) {
        return numbers.size ( );
    }

    protected abstract Collection<Integer> newCollection ( );

}
